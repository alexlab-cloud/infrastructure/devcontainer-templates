# dx.devcontainers.images

> Custom dev container definitions and images for use in other projects

## Images

| Image    | Tools                                    | Uses                       |
| -------- | ---------------------------------------- | -------------------------- |
| flagship | Docker CLI, docker-buildx, Dagger, proto | General development        |

### Using Images

Reference these images in your project's `.devcontainer.json` or `.devcontainer/devcontainer.json` file:

```json
{
    "image": "registry.gitlab.com/alexlab-cloud/dx/devcontainers/image-lib/flagship:0.1.0"
}
```

This will equip your dev environment with all of the settings and editor extensions specified in the
image's definition.
